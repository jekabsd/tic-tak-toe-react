import React, { Component } from 'react';

class Grid extends Component {

    render() {
        return (
            <div className="grid">
                {this.props.cells.map( (cell, index) => <div key={index} onClick={() => this.props.handleMove(index)} className="grid__cell">{cell === 0 ? '' : cell}</div> )}
            </div>
        )
    }
}

export default Grid