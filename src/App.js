import React, { Component } from 'react';
import Grid from './components/Grid';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isGameOver: false,
      gameEndingLabel: '',
      cells: [
        0, 0, 0, 
        0, 0, 0, 
        0, 0, 0
      ]
    }

    this.winConditions = [
      [0, 1, 2],
      [0, 4, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 4, 6],
      [3, 4, 5],
      [2, 5, 8],
      [6, 7, 8]
    ]

    this.playerOne = 'X';
    this.playerTwo = 'O';

    this.endGame = this.endGame.bind(this);
    this.startNewGame = this.startNewGame.bind(this);
    this.handleMove = this.handleMove.bind(this);
    this.didWin = this.didWin.bind(this);
  }

  startNewGame() {
    this.setState( state => {
      state.cells.forEach( (cell, index) => { state.cells[index] = 0});
      state.isGameOver = false;
      return state;
    })
  }

  endGame(gameEndingLabel) {
    this.setState( state => {
      return {
        ...state,
        isGameOver: true,
        gameEndingLabel
      };
    })
  }

  handleMove(index) {

    const player = this.state.cells.filter( cell => cell === 0).length % 2 === 0 ? this.playerOne : this.playerTwo;

      if(!this.state.isGameOver) {
          this.setState((state) => {
              
              if (state.cells[index] === 0) {
                  state.cells[index] = player;

                  if(this.didWin(player)) {
                    this.endGame(`${player} wins!`);
                  }

                  if(this.isDraw()) {
                    this.endGame('Draw');
                  }
                  
                  
              }
              return state;
          })
      }
      
  }

  didWin(player) {
      return this.winConditions.some( (condition) => {
          return condition.every((item) => this.state.cells[item] === player)
      })
  }

  isDraw() {
    return this.state.cells.every( cell => cell !== 0 );
  }


  render() {
    return (
      <div className="App">
        {this.state.isGameOver && <div className="game-ending-label">{this.state.gameEndingLabel}</div>}
        <Grid endGame={this.endGame} handleMove={this.handleMove} cells={this.state.cells}/>
        {this.state.isGameOver && <button onClick={this.startNewGame}>New game</button>}
      </div>
    );
  }
}

export default App;
